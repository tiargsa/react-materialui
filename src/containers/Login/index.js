import { connect } from 'react-redux'
import { authenticate } from '../../modules/authentication/actions'

import LoginForm from './components/LoginForm'

const mapStateToProps = (state) => ({
  failedAuthMessage: state.auth.failedAuthMessage
})

const bindActionCreators = {
  onSubmit: authenticate
}

export default connect(mapStateToProps, bindActionCreators)(LoginForm)
