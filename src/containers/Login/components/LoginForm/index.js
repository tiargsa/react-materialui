import React, { Component } from 'react'
import { Form, Field, reduxForm } from 'redux-form'
import { PropTypes } from 'prop-types'

import {RaisedButton} from 'material-ui'

import Row from 'components/Layout/Row'
import CenterContainer from 'components/Layout/CenterContainer'
import ErrorNotification from 'components/ErrorNotification'
import Input from 'components/Forms/Input'
import { required, isEmail, combineValidators } from 'components/FieldValidate'

import './style.css'
const validarRequiredyEmail = combineValidators(required, isEmail)

class Login extends Component {
  render () {
    return (
      <CenterContainer>
        <Row className='login-header'>
          <h2> Bienvenido! </h2>
        </Row>
        <Form
          onSubmit={this.props.handleSubmit(this.props.onSubmit)}
          className='center-container-login'
        >
        <Row className='login-field'>
            <Field
              component={Input}
              fullWidth
              type='text'
              name='username'
              placeholder='Email'
              validate={validarRequiredyEmail}
            />
        </Row>
        <Row className='login-field'>
            <Field
              component={Input}
              fullWidth
              type='password'
              name='password'
              placeholder='Contraseña'
              validate={required}
            />
        </Row>
        <Row  className='flex-center-row submit-buttons'>
        {this.props.failedAuthMessage &&
          <ErrorNotification message={this.props.failedAuthMessage} />
        }
        </Row>
        <Row  className='flex-center-row'>
          <RaisedButton
            type='submit'
            disabled={this.props.submiting || this.props.invalid}
          >
            Ingresar
          </RaisedButton>
        </Row>
        </Form>
      </CenterContainer>
    )
  }
}

Login.propTypes = {
  onSubmit: PropTypes.func,
  handleSubmit: PropTypes.func,
  valid: PropTypes.bool,
  failedAuthMessage: PropTypes.string
}

export default reduxForm({
  form: 'login'
})(Login)
