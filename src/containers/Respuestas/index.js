import React from 'react'
import { RaisedButton } from 'material-ui'

import './style.css'
import Row from 'components/Layout/Row'
import { downloadRespuestas } from 'services/api'
import CenterContainer from 'components/Layout/CenterContainer'

function base64ToBlob(base64, mimetype, slicesize) {
  if (!window.atob || !window.Uint8Array) {
      // The current browser doesn't have the atob function. Cannot continue
      return null;
  }
  mimetype = mimetype || '';
  slicesize = slicesize || 512;
  var bytechars = atob(base64);
  var bytearrays = [];
  for (var offset = 0; offset < bytechars.length; offset += slicesize) {
      var slice = bytechars.slice(offset, offset + slicesize);
      var bytenums = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
          bytenums[i] = slice.charCodeAt(i);
      }
      var bytearray = new Uint8Array(bytenums);
      bytearrays[bytearrays.length] = bytearray;
  }
  return new Blob(bytearrays, {type: mimetype});
};

const downloadFile = (response) => {
  var blob = base64ToBlob(response.data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=latin1');
  let a = document.createElement("a");
  a.style = "display: none";
  document.body.appendChild(a);
  let url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = 'Respuestas.xlsx';
  a.click();
  window.URL.revokeObjectURL(url);
}

const Respuestas = () => (
  <CenterContainer className='respuestas-container'>
    <Row>
      <RaisedButton
        className='main-download'
        onClick={() => downloadRespuestas().then(response => downloadFile(response))}
      >
        Descargar Respuestas
      </RaisedButton>
    </Row>
  </CenterContainer>
)

export default Respuestas
