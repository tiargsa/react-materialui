import React from 'react'
import PropTypes from 'prop-types'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

const Theme = ({children}) => (
  <MuiThemeProvider>
    {children}
  </MuiThemeProvider>
)

Theme.propTypes = {
  chlidren: PropTypes.any
}

export default Theme
