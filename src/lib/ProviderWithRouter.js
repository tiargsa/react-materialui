import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'

import configureStore from './configureStore'
import configureSagas from './configureSagas'

export default class ProviderWithRouter extends Component {
  constructor (props, context) {
    super(props)

    this.store = configureStore(configureSagas(context.router))
  }

  render () {
    return (
      <Provider store={this.store}>
        {this.props.children}
      </Provider>
    )
  }
}

ProviderWithRouter.propTypes = {
  children: PropTypes.any
}
