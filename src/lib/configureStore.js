import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk'
import rootReducer from '../store/rootReducer'

export default function (rootSaga) {
  const middleware = []
  const enhancers = []

  if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.devToolsExtension
    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension({}))
    }
  }

  const sagaMiddleware = createSagaMiddleware()
  middleware.push(sagaMiddleware)

  const store = createStore(rootReducer, compose(applyMiddleware(sagaMiddleware, thunk), ...enhancers))

  sagaMiddleware.run(rootSaga)

  // if (process.env.NODE_ENV !== 'production') {
  //   if (module.hot) {
  //     module.hot.accept('./reducers', () => {
  //       store.replaceReducer(rootReducer)
  //     })
  //   }
  // }

  return store
}
