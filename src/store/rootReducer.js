import { reducer as authReducer } from '../modules/authentication/reducer'
import { reducer as formReducer } from 'redux-form'
import { reducer as uiReducer } from '../modules/ui/reducer'

import { combineReducers } from 'redux'

export default combineReducers({
  auth: authReducer,
  form: formReducer,
  ui: uiReducer
})
