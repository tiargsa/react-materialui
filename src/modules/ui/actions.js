import constants from './constants'

export function toogleDrawer () {
  return { type: constants.DRAWER_TOOGLE }
}

export function updateDrawer (payload) {
  return { type: constants.DRAWER_UPDATE, payload }
}

export default {
  toogleDrawer,
  updateDrawer
}
