import constants from './constants'

const initialState = {
  drawer: false
}
// ERROR | ERRORMESSAGE | TYPE
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.DRAWER_TOOGLE:
      return {
        ...state,
        drawer: !state.drawer
      }
    case constants.DRAWER_UPDATE:
      return {
        ...state,
        drawer: action.payload
      }
    default:
      return state
  }
}
