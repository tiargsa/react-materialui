const AUTHENTICATE = 'AUTHENTICATE_REQUEST'
const AUTHENTICATE_SUCCESS = 'AUTHENTICATE_SUCCESS'
const AUTHENTICATE_FAILURE = 'AUTHENTICATE_FAILURE'
const TOKEN_EXPIRED = 'TOKEN_EXPIRED'
const LOGOUT = 'LOGOUT'

export default { AUTHENTICATE, AUTHENTICATE_SUCCESS, AUTHENTICATE_FAILURE, TOKEN_EXPIRED, LOGOUT }
