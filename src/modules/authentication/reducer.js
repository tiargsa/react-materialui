import constants from './constants'

const initialState = {
  fetching: false,
  isAuthenticated: false,
  failedAuthMessage: ''
}

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case constants.AUTHENTICATE_REQUEST:
      return {
        ...state,
        fetching: true,
        failedAuthMessage: ''
      }
    case constants.AUTHENTICATE_SUCCESS:
      return {
        ...state,
        fetching: false,
        authData: action.info,
        isAuthenticated: true
      }
    case constants.AUTHENTICATE_FAILURE:
      return {
        ...state,
        fetching: false,
        isAuthenticated: false,
        failedAuthMessage: action.payload
      }
    case constants.LOGOUT:
      return initialState
    default:
      return state
  }
}
