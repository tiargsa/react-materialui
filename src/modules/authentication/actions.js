import constants from './constants'
import { login } from 'services/api'


export function authenticate (credentials) {
  return function (dispatch) {
    dispatch(authenticateRequest())
    return login(credentials)
      .then(authData => {
        dispatch(authenticateSuccess(authData))
      })
      .catch(e => {
        dispatch(authenticateFailure('Su nombre de usuario y/o contraseña es incorrecto.'))
        throw e
      })
  }
}

function authenticateRequest () {
  return { type: constants.AUTHENTICATE }
}

function authenticateSuccess (info) {
  return { type: constants.AUTHENTICATE_SUCCESS, payload: info }
}

function authenticateFailure (error) {
  return {
    type: constants.AUTHENTICATE_FAILURE,
    payload: error
  }
}

export function logout () {
  return { type: constants.LOGOUT }
}

export default { authenticate, logout, authenticateSuccess, authenticateFailure }
