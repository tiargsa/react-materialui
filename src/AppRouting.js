import React from 'react'
import { Route, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Respuestas from './containers/Respuestas'
import Login from './containers/Login'

const Routes = ({ isAuthenticated }) => (
  !isAuthenticated
    ? [
      <Route
        path='/admin'
        exact
        key={1}
        component={Login}
      />
    ] : [
      <Route key={1}
        path='/admin'
        exact
        component={Respuestas}
      />
    ]
)

Routes.propTypes = {
  isAuthenticated: PropTypes.bool
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated
})

export default withRouter(connect(mapStateToProps, null)(Routes))
