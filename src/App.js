import React, { Component } from 'react'
import { BrowserRouter, withRouter } from 'react-router-dom'

import ProviderWithRouter from './lib/ProviderWithRouter'
import Theme from './lib/Theme'
import LayoutMain from './components/Layout/DashboardLayout'
import SideMenu from './components/SideMenu'
import Routes from './AppRouting'
import Alert from 'react-s-alert'

import './App.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'react-select/dist/react-select.css'
import 'react-s-alert/dist/s-alert-default.css'
import 'react-s-alert/dist/s-alert-css-effects/slide.css'

const SideMenuR = withRouter(SideMenu)

class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <Theme>
          <ProviderWithRouter>
            <SideMenuR>
              <LayoutMain >
                <Routes key={Math.random()} />
                <Alert stack={{ limit: 3 }} />
              </LayoutMain>
            </SideMenuR>
          </ProviderWithRouter>
        </Theme>
      </BrowserRouter>
    )
  }
}
export default App
