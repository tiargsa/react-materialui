import httpClient from '../httpClient'
import queryString from 'query-string'

export async function login (credentials) {
  const { data } = await httpClient.post(
    '/api/login',
    queryString.stringify(credentials)
  )

  return data
}

export async function downloadRespuestas () {
  return httpClient.get(
    '/api/respuesta'
  )
}

