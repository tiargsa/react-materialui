import axios from 'axios'
import config from './../../config'

let instance

if (process.env.NODE_ENV === 'development') {
  instance = axios
} else {
  instance = axios.create({
    baseURL: `${config[process.env.NODE_ENV].api.host}:${config[process.env.NODE_ENV].api.port}`
  })
}

export default instance
