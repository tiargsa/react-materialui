import React from 'react'
import PropTypes from 'prop-types'

const Row = ({className, style, children}) => (
  <div className={`${className} row`} style={style}>
    {children}
  </div>
)

Row.defaultProps = {
  className: ''
}

Row.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.any
}

export default Row
