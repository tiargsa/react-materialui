import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  AppBar,
  IconButton
} from 'material-ui'
import LogoutIcon from 'material-ui/svg-icons/action/exit-to-app'

import Row from './../Row'

const Container = ({children, isAuthenticated}) => (
  <div className='container-fluid'>
    <Row>
      <AppBar
        title={
        <span className='title'>
          {(
            isAuthenticated ?
            'Administración' :
            'Bahia Principe'
          )}
        </span>
        }
        iconElementRight={<IconButton><LogoutIcon /></IconButton>}
        showMenuIconButton={false}
        titleStyle={{marginLeft: 60}}
      />
      <div className='container'>
        {children}
      </div>
    </Row>
  </div>
)

Container.propTypes = {
  children: PropTypes.any
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps, null)(Container)
