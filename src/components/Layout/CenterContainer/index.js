import React from 'react'
import PropTypes from 'prop-types'

import './style.css'

const CenterContainer = ({className, style, children}) => (
  <div className={`${className} center-container`} style={style}>
    {children}
  </div>
)

CenterContainer.defaultProps = {
  className: ''
}

CenterContainer.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.any
}

export default CenterContainer
