import React from 'react'
import PropTypes from 'prop-types'
import { slide as Menu } from 'react-burger-menu'

import entradas from './entradas'

import './style.css'

class ElasticMenu extends React.Component {
  render () {
    const bmStyles = this.props.isAuthenticated ? { zIndex: 1200 } : {display: 'none'}

    return (
      <div id='outer-container'>
        <Menu
          styles={{
            bmBurgerButton: bmStyles
          }}
          isOpen={this.props.isOpen}
          onStateChange={({isOpen}) => this.props.onMenuClick(isOpen)}
          pageWrapId='page-wrap'
          outerContainerId='outer-container'
          width={300}
        >
          {entradas.map(({path, icon: Icon, name}, i) => (
            <div key={i}
              className='clickable sidebar-entry'
              onClick={() => {
                this.context.router.history.push(path)
                this.props.onMenuClick(false)
              }}
            >
              <Icon
                color='white'
                style={{
                  width: 48,
                  height: 48
                }}
              />
              <span className='sidebar-entry-name'>
                {name}
              </span>
            </div>
        ))}
        </Menu>
        <div id='page-wrap'>
          {this.props.children}
        </div>
      </div>
    )
  }
}

ElasticMenu.defaultProps = {
  isAuthenticated: true
}

ElasticMenu.contextTypes = {
  router: PropTypes.object
}

ElasticMenu.propTypes = {
  children: PropTypes.any,
  isAuthenticated: PropTypes.bool,
  isOpen: PropTypes.bool,
  onMenuClick: PropTypes.func
}

export default ElasticMenu
