import SideMenu from './SideMenu'
import uiActions from '../../modules/ui/actions'
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({
  isOpen: state.ui.drawer,
  isAuthenticated: state.auth.isAuthenticated
})

const bindActionCreators = {
  onMenuClick: uiActions.updateDrawer
}

export default connect(mapStateToProps, bindActionCreators)(SideMenu)
