// function isInt(n){
//     return Number(n) === n && n % 1 === 0;
// }

function isFloat (n) {
  return Number(n) === n && n % 1 !== 0
}

export function required (value) {
  return value === undefined || value === '' || value === null || (Array.isArray(value) && value.length === 0) ? 'Requerido' : null
}

export function validarMaximo (maximo, minimo) {
  if ((maximo !== undefined && maximo !== '') &&
    (Number(maximo) || maximo === 0) &&
    (minimo !== undefined && minimo !== '') &&
    (Number(minimo) || minimo === 0) &&
    Number(maximo) < Number(minimo)
  ) {
    return 'El máximo debe ser mayor o igual al mínimo'
  } else {
    return null
  }
}

export function validarMaximoInclusive (maximo, minimo) {
  if ((maximo !== undefined && maximo !== '') &&
    (Number(maximo) || maximo === 0) &&
    (minimo !== undefined && minimo !== '') &&
    (Number(minimo) || minimo === 0) &&
    Number(maximo) <= Number(minimo)
  ) {
    return 'El máximo debe ser mayor al mínimo'
  } else {
    return null
  }
}

export function onlyNumber (value) {
  if ((value !== undefined && value !== '') &&
    isNaN(Number(value))) {
    return 'Solo números'
  } else {
    return null
  }
}

export function onlyEnteros (value) {
  if ((value !== undefined && value !== '') &&
    isFloat(Number(value))) {
    return 'Solo enteros'
  } else {
    return null
  }
}

export function menorQue (value, parametro) {
  if ((value !== undefined && value !== '') &&
    (Number(value) || value === 0) &&
    (parametro !== undefined && parametro !== '') &&
    (Number(parametro) || parametro === 0) &&
    Number(value) >= Number(parametro)
  ) {
    return 'Debe ser menor que ' + parametro
  } else {
    return null
  }
}

export function menorQueInclusive (value, parametro) {
  if ((value !== undefined && value !== '') &&
    (Number(value) || value === 0) &&
    (parametro !== undefined && parametro !== '') &&
    (Number(parametro) || parametro === 0) &&
    Number(value) > Number(parametro)
  ) {
    return 'Debe ser menor que o igual a ' + parametro
  } else {
    return null
  }
}

export function mayorQue (value, parametro) {
  if ((value !== undefined && value !== '') &&
    (Number(value) || value === 0) &&
    (parametro !== undefined && parametro !== '') &&
    (Number(parametro) || parametro === 0) &&
    Number(value) <= Number(parametro)
  ) {
    return 'Debe ser mayor que ' + parametro
  } else {
    return null
  }
}

export function mayorQueInclusive (value, parametro) {
  if ((value !== undefined && value !== '') &&
    (Number(value) || value === 0) &&
    (parametro !== undefined && parametro !== '') &&
    (Number(parametro) || parametro === 0) &&
    Number(value) < Number(parametro)
  ) {
    return 'Debe ser mayor que o igual a ' + parametro
  } else {
    return null
  }
}

export function maxLength (value, parametro) {
  if (value !== undefined && value !== '') {
    let primerBlanco = /^ /
    let ultimoBlanco = / $/
    let variosBlancos = /[ ]+/g
    value = value.toString()
    value = value.replace(variosBlancos, ' ')
    value = value.replace(primerBlanco, '')
    value = value.replace(ultimoBlanco, '')
    if (value.length > parametro) {
      return 'El máximo es de ' + parametro + ' caracteres'
    } else {
      return null
    }
  } else {
    return null
  }
}

export function minLength (value, parametro) {
  if (value !== undefined && value !== '') {
    let primerBlanco = /^ /
    let ultimoBlanco = / $/
    let variosBlancos = /[ ]+/g
    value = value.toString()
    value = value.replace(variosBlancos, ' ')
    value = value.replace(primerBlanco, '')
    value = value.replace(ultimoBlanco, '')
    if (value.length < parametro) {
      return 'El mínimo es de ' + parametro + ' caracteres'
    } else {
      return null
    }
  } else {
    return null
  }
}

export function isEmail (value) {
  if (value !== undefined && value !== '' &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    return 'Email incorrecto'
  } else {
    return null
  }
}

export function isHour (value) {
  if (value !== undefined && value !== '' &&
    !/((?:[01]\d)|(?:2[0-3])):([0-5]\d)/i.test(value)) {
    return 'Horario incorrecto'
  } else {
    return null
  }
}

export function comparadorHoras (value, allValues) {
  if ((value !== undefined && value !== '') &&
  (allValues.horarioDesde !== undefined && allValues.horarioDesde !== '')) {
    var hora1 = new Date()
    var parts = value.split(':')
    hora1.setHours(parts[0], parts[1], 0)
    var hora2 = new Date()
    parts = allValues.horarioDesde.split(':')
    hora2.setHours(parts[0], parts[1], 0)
    if (hora1.getTime() < hora2.getTime()) return 'Debe ser mayor que ' + allValues.horarioDesde
  } else {
    return null
  }
}

export function isCoord (value) {
  if (value !== undefined && value !== '' &&
    !/((-|)\d*\.\d*)/i.test(value)) {
    return 'Valor incorrecto'
  } else {
    return null
  }
}

export function isMacAdress (value) {
  if (value !== undefined && value !== '' &&
    !/^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/i.test(value)) {
    return 'Valor incorrecto'
  } else {
    return null
  }
}

export function isUrl (value) {
  if (value !== undefined && value !== '' &&
    !/[(http(s)?)://(www)?a-zA-Z0-9@:%._~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_.~#?&//=]*)/i.test(value)) {
    return 'Url incorrecta'
  } else {
    return null
  }
}

function returnFirstOrNull (array) {
  return array.length > 0 ? array[0] : null
}

export function combineValidators (...funciones) {
  return function (value) {
    return returnFirstOrNull(funciones.map(f => f(value)).reduce((errores, vactual) => vactual !== null ? [vactual, ...errores] : errores, []))
  }
}

export default {
  required,
  validarMaximo,
  validarMaximoInclusive,
  onlyNumber,
  onlyEnteros,
  menorQue,
  menorQueInclusive,
  mayorQue,
  mayorQueInclusive,
  maxLength,
  minLength,
  isEmail,
  isCoord,
  combineValidators,
  isHour,
  comparadorHoras,
  isMacAdress,
  isUrl
}
