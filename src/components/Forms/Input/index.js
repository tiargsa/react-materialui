import React from 'react'
import PropTypes from 'prop-types'

import {
  TextField
} from 'material-ui'

const Input = ({input, meta, fullWidth, placeholder, type}) => (
  <TextField
    {...input}
    errorText={meta.touched ? meta.error : ''}
    fullWidth={fullWidth}
    hintText={placeholder}
    type={type}
  />
)

Input.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  fullWidth: PropTypes.bool,
  placeholder: PropTypes.string,
  type: PropTypes.string
}

export default Input

