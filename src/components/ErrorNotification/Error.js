import React from 'react'
import PropTypes from 'prop-types'

import './style.css'

const ErrorNotification = ({ message }) => (
  <div className='alert alert-danger alert-dismissable'>
    {message}
  </div>
)

export default ErrorNotification

ErrorNotification.propTypes = {
  message: PropTypes.string
}
